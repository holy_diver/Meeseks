﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meeseeks
{
    class Program
    {
        static string Points = "";
        static void Main(string[] args)
        {
            char K;
            for (int i = 1; i <= 2; i++)
            {
                Console.WriteLine(i.ToString() + ". noktayı girin.");
                K = Console.ReadKey().KeyChar;
                if (Points.Contains(K) || K == '0')
                {
                    Console.WriteLine("Nokta isimleri aynı olamaz ve nokta ismi 0 olamaz.");
                    i--;
                    continue;
                }
                Points += K;
                Console.WriteLine();
            }
            do
            {
                Console.WriteLine("Ek nokta girin, bitirmek için 0 girin.");
                K = Console.ReadKey().KeyChar;
                if(Points.Contains(K))
                {
                    Console.WriteLine("Nokta isimleri aynı olamaz.");
                    continue;
                }
                if (K != '0')
                {
                    Points += K;
                }
                Console.WriteLine();
            } while (K != '0');

            Console.WriteLine("Noktalar arasındaki uzaklıkları girin, eğer ki yol yoksa -1 girin.");
            double[,] Matrix = new double[Points.Length, Points.Length];
            double Length;
            for (int i = 0; i < Points.Length; i++)
            {
                for (int p = 0; p < Points.Length; p++)
                {
                    if (p == i)
                    {
                        continue;
                    }
                    if (Matrix[p, i] > 0 || Matrix[p, i] == -1)
                    {
                        Matrix[i, p] = Matrix[p, i];
                        continue;
                    }
                    Console.WriteLine(Points[i] + "->" + Points[p]);
                    do
                    {
                        Length = double.Parse(Console.ReadLine());
                        if (Length <= 0 && Length != -1)
                        {
                            Console.WriteLine("0'dan büyük bir sayı veya yol yoksa yalnızca -1 girin.");
                        }
                    } while (Length == 0);
                    Matrix[i, p] = (Length == -1) ? double.PositiveInfinity : Length;
                }
            }

            double[,] directionMatrix = new double[Points.Length, Points.Length];
            for (int i = 0; i < Points.Length; i++)
            {
                for (int p = 0; p < Points.Length; p++)
                {
                    directionMatrix[i, p] = p + 1;
                }
            }
            for (int i = 0; i < Points.Length; i++)
            {
                for (int c = 0; c < Matrix.GetLength(0); c++)
                {
                    for (int p = 0; p < Matrix.GetLength(1); p++)
                    {
                        if (c == i || p == i || Matrix[c, p] == 0)
                        {
                            continue;
                        }
                        if (Matrix[c, p] > Matrix[c, i] + Matrix[i, p])
                        {
                            Matrix[c, p] = Matrix[c, i] + Matrix[i, p];
                            directionMatrix[c, p] = i + 1;
                        }
                    }
                }
            }

            /*
            listMatrix(Matrix);
            listMatrix(directionMatrix);
            */

            while(true)
            {
                Console.WriteLine("İki noktayı girin, çıkmak için 0 girin.");
                char[] wantedPoints = new char[2];
                for (int i = 0; i < 2; i++)
                {
                    wantedPoints[i] = Console.ReadKey().KeyChar;
                    if(wantedPoints[i] == '0')
                    {
                        Environment.Exit(0);
                    }
                    Console.WriteLine();
                }
                Console.WriteLine(wantedPoints[0] + " ile " + wantedPoints[1] + " noktaları arası rota:");
                Console.Write(Matrix[Points.IndexOf(wantedPoints[0]), Points.IndexOf(wantedPoints[1])] + " birim. Yol: " + wantedPoints[0].ToString() + "->");
                int index = Points.IndexOf(wantedPoints[0]) + 1;
                do
                {
                    index = Convert.ToInt32(directionMatrix[index - 1, Points.IndexOf(wantedPoints[1])]);
                    Console.Write(Points[index-1].ToString() + "->");
                } while (index != Points.IndexOf(wantedPoints[1]) + 1);
                Console.WriteLine();
            }
        }

        static void listMatrix(double[,] Matrix)
        {
            for (int i = -1; i < Matrix.GetLength(0); i++)
            {
                if (i == -1)
                {
                    Console.Write(" ");
                    for (int c = 0; c < Matrix.GetLength(0); c++)
                    {
                        Console.Write(" " + Points[c]);
                    }
                    Console.WriteLine();
                    continue;
                }
                Console.Write(Points[i] + " ");
                for (int p = 0; p < Matrix.GetLength(1); p++)
                {
                    if (Matrix[i, p] == double.PositiveInfinity)
                    {
                        Console.Write("Y ");
                        continue;
                    }
                    Console.Write(Matrix[i, p] + " ");
                }
                Console.WriteLine();
            }
        }
    }
}
